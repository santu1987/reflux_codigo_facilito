import React, {Component} from 'react';
class PeopleStore extends Component{
	constructor(){
		super()
	}

	state = {
		peoples : []
	}
	//---------------------------------------------------
	componentDidMount(){
		fetch('https://randomuser.me/api/')
		.then(res => res.json())
		.then((data)=>{
			this.setState({ peoples: data})
		})
		.catch(console.log)
	}

}	
export default PeopleStore;