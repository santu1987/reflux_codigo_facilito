import React, {Component} from 'react';

import PeoplePhoto from './PeoplePhoto.js'
import PeopleTitle from './PeopleTitle.js'


class PeopleFrame extends Component{
	constructor(){
		super()
		this.peoples = []
		this.imagen = ""
	}
	render(){
		this.peoples = this.props.personas;
		//---
		if(this.peoples){
			//console.log(this.peoples)
			this.peoples.map(
				(elementos)=>this.imagen =elementos.picture.thumbnail,
			)
			this.peoples.map(
				(elementos)=>this.name = elementos.name
			)
				
			return(
	 			<div>
					<div>
						<PeoplePhoto imagen={this.imagen}/>
						<PeopleTitle name={this.name}/>
					</div>
	 				 				
	 			</div>
			)
		//--End if	
		}else{
			return("");
		}	
	}
}

export default PeopleFrame;