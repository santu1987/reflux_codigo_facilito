import React, {Component} from 'react';

class PeopleTitle extends Component{
	constructor(){
		super()
		this.name = ""
	}
	render(){
		this.name = this.props.name;
		return(
			<div>
				<h1>{this.name.title} {this.name.first} {this.name.last}</h1>
			</div>	
		)
	}
}

export default PeopleTitle;