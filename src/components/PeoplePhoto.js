import React, {Component} from 'react';

class PeoplePhoto extends Component{
	constructor(){
		super()
		this.imagen = ""
	}
	render(){
		this.imagen = this.props.imagen;

		return(
			<div>
				<img src={this.imagen} />
			</div>
		)
	}
}

export default PeoplePhoto;