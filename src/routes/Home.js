import React, {Component} from 'react';
//import Reflux from "reflux"
//import ReactMixin from "react-mixin"

import PeopleFrame from "../components/PeopleFrame.js";

//import PeopleActions from "../actions/PeopleActions.js";

//import PeopleStore from "../stores/PeopleStore.js"

class Home extends Component{

	state = {
		peoples : []
	}
	//---------------------------------------------------
	//De esta forma obtengo los datos a través de consumir un rest api
	componentDidMount(){
		fetch('https://randomuser.me/api/')
		.then(res => res.json())
		.then((data)=>{
			this.setState({ peoples: data})
		})
		.catch(console.log)
	}

	render(){
		return(
			<PeopleFrame personas={this.state.peoples.results} />
		);
	}	
}

export default Home;