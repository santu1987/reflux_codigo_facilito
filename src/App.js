import React from 'react';

import './assets/App.css';

import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";

import Home from './routes/Home.js';

function App() {
  return (
    <div className="">
      <header className="">
        App React Consumiendo Api REST       
      </header>
      <section className="components">
        <Router>
          <div>
            <Route path="/" component={Home} />
          </div>
        </Router>
      </section>
    </div>
  );
}

export default App;
